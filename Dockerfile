# syntax=docker/dockerfile:1 
#
# Go-Build stage
FROM golang:1.20.6-alpine3.17 as builder

# Original credit: https://github.com/jpetazzo/dockvpn
LABEL maintainer="atanarjuat <atanarjuat@riseup.net>"

ENV CGO_ENABLED=0
ENV GOOS=linux

RUN apk add --no-cache git
# this one seems to be reasonaby up-to-date.
RUN git clone https://github.com/d3vilh/openvpn_exporter

WORKDIR openvpn_exporter
RUN go build ./cmd/openvpn_exporter

WORKDIR /go
RUN git clone https://0xacab.org/leap/menshen_agent
WORKDIR menshen_agent
RUN go build .

# Smallest base image
FROM alpine:latest

COPY --from=builder /go/openvpn_exporter/openvpn_exporter /usr/local/bin
COPY --from=builder /go/menshen_agent/menshen_agent /usr/local/bin

# the functionality these dependencies provide would be nice to explore some
# day:
# openvpn-auth-pam google-authenticator pamtester libqrencode \
# Testing: pamtester
# echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing/" >> /etc/apk/repositories && \



# regular openvpn builds have no DCO support
RUN apk update && apk add --no-cache --update iptables bash openvpn easy-rsa libcap-ng linux-pam lz4-dev lzo-dev libnl3 && \
    ln -s /usr/share/easy-rsa/easyrsa /usr/local/bin && \
    rm -rf /tmp/* /var/tmp/* /var/cache/apk/* /var/cache/distfiles/*

# uncomment this to try the custom build with dco support enabled
# COPY ./apk/* /tmp/
# RUN touch repo.list && apk add --repositories-file=repo.list --allow-untrusted --no-network --no-cache /tmp/openvpn-2.6.5-r0.apk


# Needed by scripts
ENV OPENVPN=/etc/openvpn
ENV EASYRSA=/usr/share/easy-rsa \
    EASYRSA_CRL_DAYS=3650 \
    EASYRSA_PKI=$OPENVPN/pki

VOLUME ["/etc/openvpn"]

# Internally uses port 1194/udp, remap using `docker run -p 443:1194/tcp`
EXPOSE 1194/udp

CMD ["ovpn_run"]

ADD ./bin /usr/local/bin
RUN chmod a+x /usr/local/bin/*
RUN mkdir -p /var/log/openvpn
